# Firefox Terminal Toll Set (ftts)

Set of posix-like command terminal tools (following Unix tradition, small single-purpose tools that do one thing well) to completely control running Firefox instance.

The idea is to have posix commands to interact with Firefox:
 * switch tabs; 
 * navigate to URL; 
 * interact with Bookmarks, Synced Tabs, History; 
 * etc..

**Target Audience:** 

CLI-Users who wants to spend most of their time typing commands to the prompt and enjoy seeing work being done when output is printed on prompt - Matrix like and when posix commands can be used together creating own scripts to automatise repetitive tasks. CLI-Users who enjoys monitoring system using the prompt. Irrational willingness to avoid all Micro$oft technologies also motivates the CLI-Users.
